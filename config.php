<?php
    return [
        'database' => [
            'name' => 'simple_todo_app',
            'username' => 'marcovisscher',
            'password' => 'P@ssw0rd',
            'connection' => 'mysql:host=127.0.0.1',
            'charset' => 'utf8',
            'options' => [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]
        ]
    ];
?>