<?php
    /*
    *       Small explenation of how-to assemble querry's
    *
    *   To insert thing into the database we are using 3 methods for dealing with input from other functions,
    *   The first and most easy method is just using the variable passed true as show below:
    *       SELECT * FROM {$table}
    *
    *   PDO also offers a method to allow you to set placeholders, below is a quick example of this:
    *       SELECT * FROM :table
    *
    *   A second way of doing this is using question marks like this:
    *       SELECT * FROM ?
    *
    *   Here is a quick example of how to assign a vallue to said placeholder:
    *       execute('users');
    *   or incase you want to set more you simply make it an array:
    *       execute(array(':table' => 'users'));
    *   
    *   The last method is a bit more tricky and usually used when a array of information is passed true and needs to be processed.
    *
    *   We can also use sprintf to make dealing with arrays of information a bit easier in combination with the PDO.
    *   But this only works best in certain situations where the querry allows this, like the querry example below:
    *       INSERT INTO `names` ('name', 'id') VALUES ('Jack', '1')
    *
    *   With sprintf we can prepare the query with just 3 placeholders, simply because the fields and values are both in 1 area,
    *   the following example would not work that well with sprintf:
    *       UPDATE `names` SET `name` = 'Jack' WHERE `id` = '1'
    *
    *   As you can see the second example had more seperated parts in the querry, making it harder to filter the data out of an array
    *   and prepare a querry with it, in this case you most likely better of using the build in support for placeholders.
    */
    
    /*      Small Psuedo code explanation of the sprintf way of doing it.
    *
    *   We can mold it as following using the buildin 'sprintf' function:
    *       sprintf('insert into %s (%s) values (%s)',
    *       'one', 'two', 'three' );
    *
    *   Witch returns us the following result:
    *       insert into one (two) values (three)
    *
    *   So with a little change we can make it pass true what we want like so:
    *       sprintf('insert into %s (%s) values (%s)',
    *       $table, $parameters, $input );
    *
    *   To get the proper parameters in this case we are going to use the build in function array_keys,
    *   this basicly makes an array out of the information based the keys.
    *
    *   So consider the following:
    *       $app['database']->insert('users', [     // 'users' will be our $table information we pass in.
    *           'name' => 'Jack',           // 'name' is the first key in this array, and should be our first parameter but we dont want the extra stuff.
    *           'age' => '10'                         // 'age' is the second key in this array, and should be our second parameter but we dont want the extra stuff.
    *       ]);
    *       $parameters['name' = 'Jack', 'age' = '10']             // So this the array we are looking to inject, it should ignore everything else aside from the keys.
    *
    *   We dont need the whole array, just the words in a string format and we can do that using the build-in implode function,
    *   consider the following:
    *       $arr = ['one', 'two', 'three'];     // This would be the array we have,
    *       implode(', ', $arr);                // this is our implode line,
    *       one, two, three                     // is what the implode line returns.
    *
    *   We can also make a quick and dirty way to add placeholders (ex: ':name') to our querry that we replace when executed:
    *       ':' . implode(', :', array_keys($parameters))
    *
    *   This is not a proper way todo this, but its quick and easy and fits this small test so no need for array mapping etc.
    *   Simple example:
    *       $arr = ['one', 'two', 'three'];     // This would be the array we have,
    *       ':' . implode(', :', $arr);         // this is our implode line,
    *       :one, :two, :three                  // is what the implode line returns.
    *
    *   To replace the placeholders we simply use the PDO execute function as described above:
    *       $statement->execute($parameters);
    *
    *   Witch should replace them with the proper array vallues (not the keys we filtered before).
    */
    
    /* 
    *   To set the current UTC time, and set completed to vallue 1, by default for this function we need to trick the PDO statement a little bit.
    *   If we would simply use the example querry and replace only the parts we have input for (this being the id and the table),
    *   It will try to input the other details as string vallues, witch in turn screws up the UTC_TIMESTAMP.
    *   To solve this issue we set those 'extra' values in variables, and use sprintf to include them into our statement in the way they should be included.
    *   The $statement variable before we execute it looks like this (where :id is what we set with the execute):
    *       update tasks set `completed` = 1, `completed_date` = UTC_TIMESTAMP() where id = :id
    */
    
    namespace App\Core\Database;
    
    use PDO;
    
    class QueryBuilder
    {
        protected $pdo;
        
        public function __construct($pdo) {
            $this->pdo = $pdo;
        }
        
        /* Get the login details for the login process to validate the login */
        public function getLoginDetails($password, $username) {
            try {
                $statement = $this->pdo->prepare('SELECT * FROM `users` WHERE user = ? AND password = ?');
                $statement->execute(array($username, $password));
                return $statement->fetchAll(PDO::FETCH_CLASS);
            } catch (Exception $e) {
                die('Whoops, something went wrong registering your user');
            }
        }
        
        /* For people that dont have a user account we also allow them to register a new account */
        public function registerUser($username, $password) {
            try {
                $statement = $this->pdo->prepare('INSERT INTO `users` (`user`, `password`) VALUES (:uname, :pword)');
                $statement->execute(array(':uname'=>$username, ':pword'=>$password));
            } catch (Exception $e) {
                die('Whoops, something went wrong registering your user');
            }
        }
        
        /* Select all tasks from the database, trying 2 ways of dealing with placeholders */
        // To ensure we can admin any tasks, we added a check to see if the logged in account is the admin or a regular user
        public function selectAll($table, $uName) {
            if($_SESSION['login_user'] === 'admin') {
                $sql = "SELECT * FROM {$table}";
                try {
                    $statement = $this->pdo->prepare($sql);
                    $statement->execute();
                    return $statement->fetchAll(PDO::FETCH_CLASS);
                } catch (Exception $e) {
                    die('Whoops, something went wrong selecting your todo tasks');
                }
            } else {
                $sql = "SELECT * FROM {$table} WHERE `assigned_to`=:uname";
                try {
                    $statement = $this->pdo->prepare($sql);
                    $statement->execute(array(':uname' => $uName));
                    return $statement->fetchAll(PDO::FETCH_CLASS);
                } catch (Exception $e) {
                    die('Whoops, something went wrong selecting your todo tasks');
                }
            }
        }
        
        /* This is a helper function for other functions that need to check specific task properties to evaluate there actions */
        // Added a admin check on this aswell, we dont to select based on user name when the admin does something
        public function selectTask($table, $uName, $id) {
            if($_SESSION['login_user'] === 'admin') {
                $sql = "SELECT * FROM {$table} WHERE `id`=:id";
                try {
                    $statement = $this->pdo->prepare($sql);
                    $statement->execute(array(':id' => $id));
                    return $statement->fetchAll(PDO::FETCH_CLASS);
                } catch (Exception $e) {
                    die('Whoops, something went wrong selecting your todo tasks');
                }
            } else {
                $sql = "SELECT * FROM {$table} WHERE `assigned_to`=:uname AND `id`=:id";
                try {
                    $statement = $this->pdo->prepare($sql);
                    $statement->execute(array(':uname' => $uName, ':id' => $id));
                    return $statement->fetchAll(PDO::FETCH_CLASS);
                } catch (Exception $e) {
                    die('Whoops, something went wrong selecting your todo tasks');
                }
            }
        }
        
        /* Insert tasks using the sprintf way to deal with placeholders */
        public function insert($table, $data) {
            $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)',
                        $table,
                        implode(', ', array_keys($data)),
                        ':' . implode(', :', array_keys($data))
                        );
            try {
                $statement = $this->pdo->prepare($sql);
                $statement->execute($data);
            } catch (Exception $e) {
                die('Whoop something went wrong inserting you task !');
            }
        }
        
        /* Function for editing the task description */
        public function updateDescription($table, $id, $descr) {
            $sql = "UPDATE {$table} SET `description` = :descr WHERE `id` = :id";
            try {
                $statement = $this->pdo->prepare($sql);
                $statement->execute(array(':descr' => $descr, ':id' => $id));
            }catch (Exception $e) {
                die("Something went wrong trying to update a task description");
            }
        }
        
        /* Function for editing the person assigned to a task (this will remove it from the overview obviously since its userbased atm) */
        public function updateAssigned($table, $id, $assTo) {
            $sql = "UPDATE {$table} SET `assigned_to` = :assTo WHERE `id` = :id";
            try {
                $statement = $this->pdo->prepare($sql);
                $statement->execute(array(':assTo' => $assTo, ':id' => $id));
            }catch (Exception $e) {
                die("Something went wrong trying to update a task description");
            }
        }
        
        /* To complete tasks we check the completed value, and to set/reset the date we use a 0 or UTC_TIMESTAMP() */
        public function complete($table, $id, $task) {
            if($task->completed == '1') {
                $completed = '0';
                $compDate = '0';
            } else {
                $completed = '1';
                $compDate = 'UTC_TIMESTAMP()';
            }
            $sql = "UPDATE {$table} SET `completed` = {$completed}, `completed_date`= {$compDate} WHERE `id` = :id";
            try {
                $statement = $this->pdo->prepare($sql);
                $statement->execute(array(':id' => $id));
            } catch (Exception $e) {
                die('Whoops, something went wrong trying to complete your task');
            }
        }
        
        /* Remove tasks from database based on there id and assigned to vallue (not that we need 2 vallues, but its simply good practice to use 2 for delete queries) */
        public function remove($table, $data) {
            $sql = "DELETE FROM {$table} WHERE `id`=:id AND `assigned_to`=:assigned";
            try {
                $statement = $this->pdo->prepare($sql);
                $statement->execute(array('id' => $data['id'], 'assigned' => $data['assigned_to']));
            } catch (Exception $e) {
                die('Whoops, something went wrong trying to remove your task');
            }
        }
    }
?>