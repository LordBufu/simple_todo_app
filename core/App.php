<?php
    // Set Namespace
    namespace App\Core;
    
    /* Simple Dependency Injection Container.
    * We want to be able to bind the config.php to a 'key' (kinda like a global variable) in our app, somewhat like this:
    *     App:bind('config', require 'config.php');
    *
    * And then use that info like so:
    *     $config = App::get('config');
    *
    * We can do something similar for our querrybuilder like this:
    *    App::get('database', new QueryBuilder) (
    *        Connection::make(App::get('config')['database'])
    *    ));
    *    
    * Looking at the code above we would atleast need 2 function and an array,
    * a bind function that needs 2 inputs (a key and a vallue) with a protected array to store the data, and a get function with only a key to get said data.
    * The array will function as a registry storage container, each time we bind we store it in the protected registry array.
    * 
    * To bind to the registry array we can do this (since the function and array are both static):
    *     static::$registry[$key] = $value;
    *
    * So if we would use the config.php to connect to our DB and we use a querry to find something the array would look like this:
    *     $registry = [
    *         'config' => [],                 // With data normally obviously
    *         'database' => $querryBuilder
    *         ]
    *
    * As you can see each bind will be linked with its own key, and new binds of the same function will overwrite the previous.
    *
    * To return the get information you could do something like this:
    *     return static::$register[$key];
    *        
    * But you might want to check if there is a key to begin with, and throw and exception if there isnt:
    *     if(! array_key_exists($key, static::$registry)) {
    *         throw new Exception("No {$key} is bound in the container");
    *     }
    * 
    * Now to simply explain it once more, this is a simple dependency injection container, you can bind and fetch anything to the registry like so for example:
    *     App::bind('foo', 'bar');
    *     die(App::get('foo'));           // This will return 'bar' since that is stored with the key 'foo'.
    */
    
    // Define App class
    class App {
        // The protected empty registry array
        protected static $registry = [];
        
        // The bind function
        public static function bind($key, $value) {
            static::$registry[$key] = $value;
        }
        
        // The get function
        public static function get($key) {
            if (! array_key_exists($key, static::$registry)) {
                throw new Exception("No {$key} is bound in the container.");
            }
            
            return static::$registry[$key];
        }
    }
?>