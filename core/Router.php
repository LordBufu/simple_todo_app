<?php
    /*
    *   GET/POST Functions explained:
    *
    *   The way GET and POST works is as following:
    *       $getRoutes = [];
    *       $getRoutes[] = 'foo';
    *       $getRoutes[] = 'bar';
    *       $getRoutes['uri'] = 'baz'
    *
    *   Which then makes the routes look as following:
    *       [
    *           0 => 'foo',
    *           1 => 'bar'
    *           'uri' => 'baz'
    *       ]
    *
    *   So incase of foo and bar the router would direct you to:
    *       0/foo.php or 1/bar.php
    *
    *   That is obviously not the request we want, so we include the $uri to shape it properly,
    *   witch for our baz example would make the line as following.
    *       $getRoutes['uri'] = controllers/baz.php
    *
    *   The uri is replaced with what is passed into the request.php uri function, so for example 'names' or 'completed' etc.
    */
    
    /*
    *   Direct Function Explained:
    *   We first check if there is a route for the uri + requestType we got from the browser.
    *
    *   The request would look something like this:
    *       'PagesController@home'
    *
    *   Consider the following example:
    *       var_dump(explode('@', 'PagesController@home'));
    *
    *   This would return an array with the following content:
    *       ['PagesController', 'home']
    *
    *   So we can use the explode function to split the incomming data, so we can determine what route is needed.
    *   We dont really want an array, so we are going to use the splat operator (php 5.6 function) to make something like this:
    *       var_dump(...explode('@', 'PagesController@home'));
    *
    *   This would seperate them into agruments, and we can pass this into our callAction function.
    *
    *   Debug-code Snippets:
    *       die(var_dump($uri, $requestType)); // Debug line to check the variables passed in
    *       die(var_dump($uri, $this->routes[$requestType])); // Debug current route against all routes.
    *       die(var_dump($this->callAction(...explode('@', $this->routes[$requestType][$uri]))) // Debug if the route is passed correctly
    */
        
    /* Set namespace */
    namespace App\Core;

    /* Define router class */
    class Router {
        /* Protect the GET/POST routes */
        protected $routes = [
            'GET' => [],
            'POST' => []
        ];
        
        /* Load function to load the routes file and store it in the array */
        public static function load($file) {
            $router = new static;
            require $file;
            return $router;
        }
        
        /* GET request function */
        public function get($uri, $controller) {
            $this->routes['GET'][$uri] = $controller;
        }
        
        /* POST request function */
        public function post($uri, $controller) {
            $this->routes['POST'][$uri] = $controller;
        }
        
        public function direct($uri, $requestType) {
            if (array_key_exists($uri, $this->routes[$requestType])) {
                return $this->callAction(
                    ...explode('@', $this->routes[$requestType][$uri])
                );
            }
            throw new Exception('No route defined for this URI.');
        }
        
        /* callAction function to help navigate traffic for the direct function */
        protected function callAction($controller, $action) {
            $controller = "App\\Controllers\\{$controller}";
            $controller = new $controller;
            if (! method_exists($controller, $action)) {
                throw new Exception("{$controller} does not respond to the {$action} action.");
            }
            return $controller->$action();
        }
    }
?>