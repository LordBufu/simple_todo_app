<?php
    /* Use Namespaces */
    use App\Core\App;
    use App\Core\Database\{QueryBuilder, Connection};
    
    // Bind config to registry
    App::bind('config', require 'config.php');

    // Bind request querry to registry
    App::bind('database', new QueryBuilder (
        Connection::make(App::get('config')['database'])
    ));
?>