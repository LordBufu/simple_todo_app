<?php
    /* very simple redirect function */
    function redirect($path) {;
        header("location: /{$path}");
    }
    
    /*
        Extract is basiclly the oposite of the compact we used in the controller.
        Consider the following as an example:
            $data = ['name' => 'joe', 'age' => '25'];
        Would become:
            $name = 'joe';
            $age = '25';
        
        We default $data to a empty array since we wont always have data to pass true
    */
    
    function view($name, $data = []) {
        extract($data);
        return require "App/Views/{$name}.view.php";
    }
?>