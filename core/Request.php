<?php
    namespace App\Core;
    
    class Request
    {
        /*
        * The first part (trim) removes the '/' from the requested line,
        * so if we would give the app a name via a post method we would end up with the following:
        *    "/names?name=UserInput" would become "names?name=UserInput"
        * Not quite something we can work with for the router, so we need to filter it in a different way,
        * with the parse_url function we can be a lot more specific and filter that same request_uri to only have the part we want (in this case the php_url_path).
        * For more information about he specific filter visit the PhP site on request_uri.
        *     With parse_url and trimmming of the '/' we now return "names" to the router.
        */
        
        public static function uri()
        {
            return trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
        }
        
        /*
        * To get the proper method we can use some of the previuos code to get the right part of the request,
        * so instead of getting the request_uri we simply filter the request_method.
        */
        public static function method()
        {
            return $_SERVER['REQUEST_METHOD'];
        }
    }
?>