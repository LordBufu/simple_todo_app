sudo composer self-update

# PHP STUFFsudo apt-get update -y
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update -y

sudo apt-get install php7.0-curl php7.0-cli php7.0-dev php7.0-gd php7.0-intl php7.0-mcrypt php7.0-json php7.0-mysql php7.0-opcache php7.0-bcmath php7.0-mbstring php7.0-soap php7.0-xml php7.0-zip php7.0-sqlite3 -y

sudo a2dismod php5
sudo apt-get install libapache2-mod-php7.0 -y
sudo apt-get install php-xdebug

#install phpmyadmin
phpmyadmin-ctl install

#starting mysql
mysql-ctl start

# maak .htaccess voor router course
touch .htaccess
echo -e "RewriteEngine On" >> .htaccess
echo -e "RewriteRule ^.*$ index.php [END]" >> .htaccess

# maak de index.php
touch index.php
echo -e "<?php \n\necho 'Hello world';" >> index.php