1. Druk op het groene plusje naast de bash en Immediate tabs (helemaal onderaan) een nieuw tab 'New Run Configuration'
2. Klik op (rechts in de nieuwe tab) Runner:Auto naar Shell script
3. Vul bij Command de naam 'install.sh' in en druk op Run
4. Het script gaat de nieuwste PHP installeren, dat koste even wat tijd
5. Als alles klaar is druk op Run Project boven aan in Cloud9 en open de URL die je onderaan in een tab ziet