<?php
    require 'vendor/autoload.php';
    require 'core/bootstrap.php';
    require 'core/Helpers.php';
    
    use App\Core\{Router, Request};
    
    Router::load('App/routes.php')
        ->direct(Request::uri(), Request::method());
?>

<!-- Loading all assets that makes-up the page -->
<link rel='stylesheet' type='text/css' href='/public/css/style.css'>