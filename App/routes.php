<?php
    /*
    *   Since we are using a PageController now, we need to make sure our routes are set the right path.
    *   So our homepage used to be:
    *       $router->get('', 'controllers/index.php');
    *   With the PageController class it turns into this:
    *       $router->get('', 'PagesController@home');
    *
    *   Dump all routes to check if the routes are loaded properly, requires router '$routes' to be set o public instead of protected.
    *       var_dump($router->routes);
    */
    
    // The homepage routes to ensure all redirects work properly
    $router->get('', 'PagesController@home');
    $router->get('index', 'PagesController@home');
    
    // All routes for the user account related features
    $router->get('registerForm', 'UsersController@registerForm');
    $router->post('register', 'UsersController@register');
    $router->get('loginForm', 'UsersController@loginForm');
    $router->post('login', 'UsersController@login');
    $router->get('logout', 'UsersController@logout');
    
    // All routes for modifying tasks on the list
    $router->post('addTask', 'PagesController@addTask');
    $router->post('editDescription', 'PagesController@editDescription');
    $router->post('editAssigned', 'PagesController@editAssigned');
    $router->post('notCompleted', 'PagesController@completeTask');
    $router->post('completed', 'PagesController@completeTask');
    $router->post('removeTask', 'PagesController@removeTask');
?>