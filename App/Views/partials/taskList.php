<h1> Taken Lijst </h1>

<!--  Simple table design to display all tasks in the database -->
<table style="width:75%">
    <tr>
        <th> Task-ID </th>
        <th> Description </th>
        <th> Assigned to </th>
        <th> Assigned on </th>
        <th> Completed </th>
        <th> Completed Date </th>
        <th> Remove Task </th>
    </tr>
        <!-- Our own form to add a task to the list -->
    <tr>
        <form id="addTask" action="/addTask" method="Post">
            <!-- The submit button goes in place of the task id --> 
            <td><center>
                <input id="addTask" type="submit" value="Submit Task">
            </center></td>
            <!-- The description field -->
            <td><center>
                <input id="addTask" name="description" placeholder="Task Description" type="text" autocomplete="off">
            </center></td>
            <!-- The assigned to field -->
            <td><center>
                <input id="addTask" name="assigned_to" placeholder="Assigned To" type="text" autocomplete="off">
            </center></td>
            <!-- Empty field because we dont allow people to set date and time fields -->
            <td><center></center></td>
            <!-- The completed to field -->
            <td><center>
                <select id="addTask" name="completed">
                    <option value="1"> Yes </option>
                    <option value="2"> No </option>
                </select>
            </center></td>
            <!-- Empty field because we dont allow people to set date and time fields -->
            <td><center></center></td>
            <!-- Empty field because we dont want to delete but we want to add -->
            <td><center>
                <input id="addTask" type="reset" value="Reset input">
            </center></td>
        </form>
    </tr>
<?php foreach ($tasks as $task) : ;?>
    <tr>
        <!-- This field that cant be edited, the id of task (or a id in an DB in general) are mostliky unqiue auto_increment key vallues that you never want to edit yourself -->
        <td><center> <?= $task->id; ?> </center></td>
        
        <!-- We allow user input on the desciption field, we display the current description as a placeholder and send the id as hidden input -->
        <td><center>
            <form action="/editDescription" method="POST">
                <input id="editDescription" name="description" placeholder="<?= $task->description; ?>" type="text" autocomplete="off">
                <input id="editDescription" name="id" value="<?= $task->id; ?>" type="hidden">
            </form>
        </center></td>
        
        <!-- We allow user input on the assigned_to field, we display the current assigned_to as a placeholder and send the id as hidden input -->
        <td><center>
            <form action="/editAssigned" method="POST">
                <input id="editAssigned" name="assigned_to" placeholder="<?= $task->assigned_to; ?>" type="text" autocomplete="off">
                <input id="editAssigned" name="id" value="<?= $task->id; ?>" type="hidden">
            </form>
        </center></td>
        
        <!-- This field that cant be edited, we dont want people changing the dates we fill in automaticlly -->
        <td><center> <?= $task->assign_date; ?> </center></td>
        
        <!-- We check if the task is flagged as completed or not, and display the correct symbol where needed. -->
        <td><center>
            <?php if($task->completed == '1'){ ?>
                <form action="/notCompleted" method="POST">
                    <button type="submit" name="id" value="<?= $task->id; ?>"> &#10004; </button>
                </form>
            <?php } else{ ?>
                <form action="/completed" method="POST">
                    <button type="submit" name="id" value="<?= $task->id; ?>"> &#10007; </button>
                </form>
            <?php } ?>
        </center></td>
        
        <!-- This field that cant be edited, we dont want people changing the dates we fill in automaticlly -->
        <td><center> <?= $task->completed_date; ?> </center></td>
        
        <!-- To remove tasks from the list we offer a button inside the table, and send the task id back in the POST request. -->
        <td><center>
            <form action="/removeTask" method="POST">
                <button type="submit" name="id" value="<?= $task->id; ?>"> &#10007; </button>
            </form>
        </center></td>
    </tr>
<?php endforeach; ?>
</table>