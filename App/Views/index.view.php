<center>
<?php
require('partials/head.php');

if(isset($_SESSION['login_user'])) {
    require('partials/nav_logedin.php');
    require('partials/taskList.php');
} else {
    require('partials/nav_logedout.php');
}

require('partials/footer.php');

?>
</center>