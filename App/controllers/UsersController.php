<?php
    namespace App\Controllers;
    
    use App\Core\App;
    
    class UsersController {
        public function registerForm() {
            return view('register');
        }
        
        public function register() {
            /* Not sure if i need this, just leaving it here as an experimental feature for now, should check if there is any input to begin with */
            foreach($_POST as $key=>$value) {
	            if(empty($_POST[$key])) {
	                die('All Fields are required');
	                break;
	           }
            }
            
            /* Password Matching Validation */
            if($_POST['password'] != $_POST['confirm_password']){ 
                die('Passwords should be same'); 
            }
            /* Now that we checked everything, we should encrypt the password and store it */
            else {
                $username = $_POST['username']; // setting username just incase
                $password = sha1($_POST['password']); // very simple and low lvl encrypt
                try {
                    App::get('database')->registerUser($username, $password);
                }
                catch (Exception $e) {
                    die('Register process failed');
                }
                return view('index');
            }
        }
        
        public function loginForm() {
            return view('login');
        }
        
        public function login() {
            session_start();
            
            /* Lets check if we got a username and password true the post request */
            if (empty($_POST['username']) || empty($_POST['password'])) {
                die('You cant login without giving us your login details');
            }
            else
            {
                /* Store post info into variable, mainly to encrypt the password */
                $username = $_POST['username'];
                $password = sha1($_POST['password']);
                
                /* Search the database for a match in username and password */
                $userdetails = App::get('database')->getLoginDetails($password, $username);
                
                /* Checking if we got a match and initialize the session if there is */
                if(empty($userdetails)) {
                    die('You cant login without giving us your login details');
                }
                else {
                    $_SESSION['login_user'] = $username; // Initializing Session
                    return redirect('index');
                }
            }
        }
        
        /* Easy and clean logout function, takes the current user session and will only unset that session. */
        public function logout() {
            session_start();
            
            if(isset($_SESSION['login_user'])) {
                unset($_SESSION['login_user']);
                return view('index');
            }
        }
    }
?>