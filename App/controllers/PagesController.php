<?php
    /*      How the PageControllers work.
    *
    *   The basic concept is very simple, a controller should realisticly only take a request and process it.
    *   Meaning we take the GET/POST etc requests, and determine where to route it and what function we want to pass the information into that we recieved.
    *   Processing the information should always be done by the function we call, this way we keep the controller nice and clean for trouble-shooting.
    *
    *   This line loads the variable needed for the partial taskList.php to display the task list:
    *       $tasks = App::get('database')->selectAll('tasks');
    *
    *   The concept of returning a variable using our own view function is as following:
    *       return view('index', ['tasks' => $tasks]);
    *    
    *   We have a function in PhP for this, but its not very clear to read what is happening:
    *       return view('index', compact('tasks'));
    *
    *   Both ways of doing this give a similar end result, but compact seems to be the most common way to do it, and fits quite well in smaller projects like this one.
    */
    
    namespace App\Controllers;
    
    use App\Core\App;
    
    class PagesController {
        /* Controller for all homepage requests, this also deal with displaying the tasks list using the $tasks variable */
        public function home() {
            session_start();
            $tasks = App::get('database')->selectAll('tasks', $_SESSION['login_user']);
            return view('index', compact('tasks'));
        }

        /* Controller for loading the addTask view, and pass true the current tasks for the taskList partial */
        public function addTask() {
            session_start();
            App::get('database')->insert('tasks', $_POST);
            return redirect('index');
        }
        
        /* Very simple function to update the description of a task */
        public function editDescription() {
            session_start();
            App::get('database')->updateDescription('tasks', $_POST['id'], $_POST['description']);
            return redirect('index');
        }
        
        /* Very simple function to update the assigned person of a task */
        public function editAssigned() {
            session_start();
            App::get('database')->updateAssigned('tasks', $_POST['id'], $_POST['assigned_to']);
            return redirect('index');
        }
        
        /* Pass all input to the QuerryBuilder, and redirect back to the index */
        public function completeTask() {
            session_start();
            $task = App::get('database')->selectTask('tasks', $_SESSION['login_user'], $_POST['id']);
            App::get('database')->complete('tasks', $_POST['id'], $task[0]);
            return redirect('index');
        }
        
        /* Controller to remove tasks, and redirect to the index */
        public function removeTask() {
            session_start();
            App::get('database')->remove('tasks', [
                'id' => $_POST['id'],
                'assigned_to' => $_SESSION['login_user']
            ]);
            return redirect('index');
        }
    }
?>